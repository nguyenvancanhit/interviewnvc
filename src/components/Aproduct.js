/**
 * Created by VanCanh on 3/11/2018.
 */
/**
 * Created by VanCanh on 3/9/2018.
 */
'use strict';

import React from 'react';
import { render } from 'react-dom';
import { Link } from 'react-router';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import StarBorder from 'material-ui-icons/StarBorder';
import Star from 'material-ui-icons/Star';
import moment from 'moment';

export default class Aproduct extends React.Component {
    constructor(props) {
        super(props);
        var productDatas = [];

        this.state = {
            time: 0
        };
    };
    render() {
        var that= this;
        setInterval(function(){
            var datestr = new Date();
            var date = moment(datestr,"HH:mm:s");
            that.state.time = date.format("HH:mm:s");
            that.setState({time:  that.state.time });
        }, 1000);

        return (
            <Link to={`/productDetail/${this.props.id}`}>
            <div className="a-product">
                <span className="img-deal-badge">-{Math.floor(((this.props.marketPrice - this.props.price)/this.props.marketPrice) * 100) }%</span>
                <span> <img width="24" height="24" className="img-2h-circle" src="/imgs/2h-circle@2x.png"/></span>
                <Grid container alignItems="center" direction="column" justify="flex-start">
                    <Grid item>
                        <div>
                            <img width="200" height="200" src={'/imgs/' +this.props.imgAvatar} />
                        </div>
                    </Grid>
                    <Grid item>
                        <div className="title-aproduct">
                            {this.props.name}
                        </div>
                        <div className="margin-top-10">
                            <Grid container alignItems="center" direction="row" justify="flex-start">
                                <Grid item>
                                    { this.props.star <5 && [...Array(this.props.star)].map((x, i) =><Star className="star" ></Star>)
                                    }
                                    {
                                        this.props.star <5 && [...Array(5 - this.props.star)].map((x, i) =><StarBorder className="star-border"></StarBorder>)
                                    }
                                    {
                                        this.props.star >4 && [...Array(5)].map((x, i) =><Star className="star"></Star>)
                                    }

                                </Grid>
                                <Grid item>
                                    <span className="comment"> ({this.props.comment} nhận xét)</span>
                                </Grid>
                            </Grid>
                        </div>
                        <div>
                            <Grid container alignItems="center" direction="row" justify="flex-start">
                                <Grid item className="price-sale">
                                     <span>{this.props.price} </span><span>đ</span>
                                </Grid>
                                <Grid item className="price-regular">
                                    <span>{this.props.marketPrice} </span><span>đ</span>
                                </Grid>
                            </Grid>
                        </div>
                        <div className="process-bar"><div></div></div>
                        <div className="margin-top-10">
                            <Grid container alignItems="center" direction="row" justify="space-between">
                                <Grid item className="deal-count-down">
                                    <span>Deal còn lại: 90 %</span>
                                </Grid>
                                <Grid item className="deal-count-down">
                                    <span>Kết thúc sau: <span className="time-count-down">{this.props.dateEnd} ngày {this.state.time}</span> </span>
                                </Grid>
                            </Grid>
                        </div>
                    </Grid>
                </Grid>
            </div>
           </Link>
        );
    }
}
