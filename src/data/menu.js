"use strict";

const menuDatas =
    [
        {
            "type": "Điện thoại - máy tính bảng",
            "level": 1,
            "image": "/imgs/7129693f024f7be24f0ecc7de55eebdd.png",
            "imageCategory": "631b937017552bf03d0629ea9a5bb1d9.jpg",
            "icon": "ic_phone_iphone_black_24dp_1x.png",
            "data": [
                {
                    "group": "Xu hướng & Nổi bật",
                    "data": [
                        {
                            "name": "Smartphone dưới 3 triệu",
                            "level_1": 1,
                            "level_2": 1
                        },
                        {
                            "name": "Điện thoại dưới 1 triệu",
                            "level_1": 1,
                            "level_2": 2
                        },
                        {
                            "name": "IPhone",
                            "level_1": 1,
                            "level_2": 3
                        },
                        {
                            "name": "IPad Pro - IPad New",
                            "level_1": 1,
                            "level_2": 4
                        },
                        {
                            "name": "Samsung Galaxy A8 2018",
                            "level_1": 1,
                            "level_2": 5
                        },
                        {
                            "name": "Xiaomi Redmi Note 4",
                            "level_1": 1,
                            "level_2": 6
                        },
                        {
                            "name": "Sony XZ Premium",
                            "level_1": 1,
                            "level_2": 7
                        },
                        {
                            "name": "HTC 10 Evo",
                            "level_1": 1,
                            "level_2": 8
                        },
                        {
                            "name": "Huawei GR5 2017",
                            "level_1": 1,
                            "level_2": 9
                        },
                        {
                            "name": "Asus Zenfone 4 Max Pro",
                            "level_1": 1,
                            "level_2": 10
                        }
                    ]
                },
                {
                    "group": "Smartphone chính hãng",
                    "data": [
                        {
                            "name": "Apple",
                            "level_1": 1,
                            "level_2": 11
                        },
                        {
                            "name": "Nokia",
                            "level_1": 1,
                            "level_2": 12
                        },
                        {
                            "name": "Xiaomi",
                            "level_1": 1,
                            "level_2": 13
                        },
                        {
                            "name": "Oppo",
                            "level_1": 1,
                            "level_2": 14
                        },
                        {
                            "name": "Sony",
                            "level_1": 1,
                            "level_2": 15
                        },
                        {
                            "name": "Huawei",
                            "level_1": 1,
                            "level_2": 16
                        },
                        {
                            "name": "Apple",
                            "level_1": 1,
                            "level_2": 17
                        },
                        {
                            "name": "Asus",
                            "level_1": 1,
                            "level_2": 18
                        }
                    ]
                },
                {
                    "group": "Lựa Chọn Mức Giá",
                    "data": [
                        {
                            "name": "Dưới 3 Triệu",
                            "level_1": 1,
                            "level_2": 19
                        },
                        {
                            "name": "3 Triệu - 8 Triệu",
                            "level_1": 1,
                            "level_2": 20
                        },
                        {
                            "name": "Trên 8 Triệu",
                            "level_1": 1,
                            "level_2": 21
                        }
                    ]
                }
            ]
        },
        {
            "type": "TV - Thiết Bị Nghe Nhìn",
            "image": "/imgs/5519d1cbda60b753f9382ed7d85d8c77.jpg",
            "imageCategory": "f16b45138bf18ab0746d02600647eb6e.jpg",
            "icon": "ic_personal_video_black_24dp_1x.png",
            "level": 2,
            "data": [
                {
                    "group": "Thương Hiệu Tivi",
                    "data": [
                        {
                            "name": "Tivi Sony",
                            "level_1": 2,
                            "level_2": 1
                        },
                        {
                            "name": "Tivi Samsung",
                            "level_1": 2,
                            "level_2": 2
                        },
                        {
                            "name": "Tivi LG",
                            "level_1": 2,
                            "level_2": 3
                        },
                        {
                            "name": "Tivi Panasonic",
                            "level_1": 2,
                            "level_2": 4
                        },
                        {
                            "name": "Tivi Toshiba",
                            "level_1": 2,
                            "level_2": 5
                        }
                    ]
                },
                {
                    "group": "Âm thanh và Phụ kiện",
                    "data": [
                        {
                            "name": "Dàn Âm Thanh",
                            "level_1": 2,
                            "level_2": 6
                        },
                        {
                            "name": "Loa Karaoke",
                            "level_1": 2,
                            "level_2": 7
                        },
                        {
                            "name": "Giá Treo Tivi",
                            "level_1": 2,
                            "level_2": 8
                        },
                        {
                            "name": "Amply Nghe Nhạc",
                            "level_1": 2,
                            "level_2": 9
                        }
                    ]
                },
                {
                    "group": "Kích Thước Màn Hình",
                    "data": [
                        {
                            "name": "24 inch - 28 inch",
                            "level_1": 2,
                            "level_2": 10
                        },
                        {
                            "name": "29 inch- 40 inch",
                            "level_1": 2,
                            "level_2": 11
                        },
                        {
                            "name": "Trên 40 inch",
                            "level_1": 2,
                            "level_2": 12
                        }
                    ]
                }
            ]
        },
        {
            "type": "Phụ Kiện - Thiết Bị Số",
            "image": "/imgs/cdfedde5344021cb5f23bace2bca67b2.jpg",
            "imageCategory": "fe155d76f9859e414a243f399ac090e2.jpg",
            "icon": "ic_headset_black_24dp_1x.png",
            "level": 3,
            "data": [
                {
                    "group": "Thiết Bị Âm Thanh",
                    "data": [
                        {
                            "name": "Tai nghe nhét tai",
                            "level_1": 3,
                            "level_2": 1
                        },
                        {
                            "name": "Tai nghe chụp tai",
                            "level_1": 3,
                            "level_2": 2
                        },
                        {
                            "name": "Tai nghe nhạc Bluetooth đàm thoại",
                            "level_1": 3,
                            "level_2": 3
                        },
                        {
                            "name": "Tai nghe nhạc Bluetooth thể thao",
                            "level_1": 3,
                            "level_2": 4
                        },
                        {
                            "name": "Tai nghe nhạc Bluetooth",
                            "level_1": 3,
                            "level_2": 5
                        }
                    ]
                },
                {
                    "group": "Thiết Bị Game",
                    "data": [
                        {
                            "name": "Chuột Gaming",
                            "level_1": 3,
                            "level_2": 6
                        },
                        {
                            "name": "Bàn phím Gaming",
                            "level_1": 3,
                            "level_2": 7
                        },
                        {
                            "name": "Ghế Gaming",
                            "level_1": 3,
                            "level_2": 8
                        },
                        {
                            "name": "Lót chuột Gaming",
                            "level_1": 3,
                            "level_2": 9
                        }
                    ]
                },
                {
                    "group": "Thiết Bị Mạng",
                    "data": [
                        {
                            "name": "Bộ phát wifi",
                            "level_1": 3,
                            "level_2": 10
                        },
                        {
                            "name": "Thiết bị 3G/4G",
                            "level_1": 3,
                            "level_2": 11
                        },
                        {
                            "name": "USB wifi",
                            "level_1": 3,
                            "level_2": 12
                        }
                    ]
                }
            ]
        },
        {
            "type": "Laptop - Thiết Bị IT",
            "image": "/imgs/7129693f024f7be24f0ecc7de55eebdd.png",
            "icon": "ic_laptop_mac_black_24dp_1x.png",
            "imageCategory": "64f510d6d9a54f6ceb5e65fef2cba2cf.jpg",
            "level": 4,
            "data": [
                {
                    "group": "Laptop Chính Hãng",
                    "data": [
                        {
                            "name": "Macbook",
                            "level_1": 4,
                            "level_2": 1
                        },
                        {
                            "name": "Dell",
                            "level_1": 4,
                            "level_2": 2
                        },
                        {
                            "name": "Asus",
                            "level_1": 4,
                            "level_2": 3
                        },
                        {
                            "name": "HP",
                            "level_1": 4,
                            "level_2": 4
                        },
                        {
                            "name": "Lenovo",
                            "level_1": 4,
                            "level_2": 5
                        }
                    ]
                },
                {
                    "group": "Thiết Bị Văn Phòng",
                    "data": [
                        {
                            "name": "Màn hình máy tính",
                            "level_1": 4,
                            "level_2": 6
                        },
                        {
                            "name": "Máy chiếu",
                            "level_1": 4,
                            "level_2": 7
                        },
                        {
                            "name": "Mực in",
                            "level_1": 4,
                            "level_2": 8
                        },
                        {
                            "name": "Giấy in",
                            "level_1": 4,
                            "level_2": 9
                        }
                    ]
                },
                {
                    "group": "Phụ kiện - Linh Kiện",
                    "data": [
                        {
                            "name": "Bộ Lưu Điện",
                            "level_1": 4,
                            "level_2": 10
                        },
                        {
                            "name": "Đế tản nhiệt laptop",
                            "level_1": 4,
                            "level_2": 11
                        },
                        {
                            "name": "RAM",
                            "level_1": 4,
                            "level_2": 12
                        }
                    ]
                }
            ]
        },
        {
            "type": "Máy Ảnh - Quay Phim",
            "image": "/imgs/f8f84151a4d59d9038cc31f15099a6c8.jpg",
            "icon": "ic_photo_camera_black_24dp_1x.png",
            "imageCategory": "e1556a55bb90b9ec585c05da7fc5b646.jpg",
            "level": 5,
            "data": [
                {
                    "group": "Máy Ảnh",
                    "data": [
                        {
                            "name": "Máy ảnh DSLR",
                            "level_1": 5,
                            "level_2": 1
                        },
                        {
                            "name": "Máy ảnh du lịch",
                            "level_1": 5,
                            "level_2": 2
                        },
                        {
                            "name": "Máy ảnh chụp lấy liền",
                            "level_1": 5,
                            "level_2": 3
                        },
                        {
                            "name": "Máy ảnh mirrorless",
                            "level_1": 5,
                            "level_2": 4
                        }
                    ]
                },
                {
                    "group": "Máy Quay Phim",
                    "data": [
                        {
                            "name": "Máy quay phim sony",
                            "level_1": 5,
                            "level_2": 5
                        },
                        {
                            "name": "Máy quay phim canon",
                            "level_1": 5,
                            "level_2": 6
                        },
                        {
                            "name": "Lens Canon",
                            "level_1": 5,
                            "level_2": 7
                        },
                        {
                            "name": "Lens Sony",
                            "level_1": 5,
                            "level_2": 8
                        }
                    ]
                },
                {
                    "group": "Thương Hiệu",
                    "data": [
                        {
                            "name": "Canon",
                            "level_1": 5,
                            "level_2": 9
                        },
                        {
                            "name": "Nikon",
                            "level_1": 5,
                            "level_2": 10
                        },
                        {
                            "name": "Sony",
                            "level_1": 5,
                            "level_2": 11
                        },
                        {
                            "name": "Pixel",
                            "level_1": 5,
                            "level_2": 12
                        }
                    ]
                }
            ]
        },
        {
            "type": "Điện Gia Dụng - Điện Lạnh",
            "image": "/imgs/d0117117b61f90d8990ef4e7e17565c9.jpg",
            "icon": "ic_settings_input_hdmi_black_24dp_1x.png",
            "imageCategory": "d0117117b61f90d8990ef4e7e17565c9.jpg",
            "level": 6,
            "data": []
        },
        {
            "type": "Nhà Cửa Đời Sống",
            "image": "/imgs/0a31c89437b0a70df3f81cab855f8c75.jpg",
            "icon": "ic_store_black_24dp_1x.png",
            "imageCategory": "0a31c89437b0a70df3f81cab855f8c75.jpg",
            "level": 7,
            "data": []
        },
        {
            "type": "Hàng Tiêu Dùng - Thực Phẩm",
            "image": "/imgs/f4e3a1c99139fb725b74956bb3abbbcb.jpg",
            "icon": "ic_local_dining_black_24dp_1x.png",
            "imageCategory": "f4e3a1c99139fb725b74956bb3abbbcb.jpg",
            "level": 8,
            "data": []
        },
        {
            "type": "Đồ Chơi Mẹ Và Bé",
            "image": "/imgs/8b0d73ff1051d93346e4a9add33cddef.jpg",
            "icon": "ic_pregnant_woman_black_24dp_1x.png",
            "imageCategory": "8b0d73ff1051d93346e4a9add33cddef.jpg",
            "level": 9,
            "data": []
        },
        {
            "type": "Xe Máy - Ôtô - Xe Đạp",
            "image": "/imgs/2d9ae8c96f1df0d0cf4f0abece321d6c.jpg",
            "icon": "ic_motorcycle_black_24dp_1x.png",
            "imageCategory": "2d9ae8c96f1df0d0cf4f0abece321d6c.jpg",
            "level": 10,
            "data": []
        },
        {
            "type": "Sách, VPP & Qùa Tặng",
            "image": "/imgs/87c2a8340a340f4c60ffc87cccc15573.jpg",
            "imageCategory": "87c2a8340a340f4c60ffc87cccc15573.jpg",
            "icon": "ic_library_books_black_24dp_1x.png",
            "level": 11,
            "data": []
        }
    ]

export default menuDatas;
