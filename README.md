# README #

This README would normally document whatever steps are necessary to get your application up and running.
##Owner ###
* Nguyễn Văn Cảnh - 0907593182
### What is this repository for? ###

* Interview with Tiki company
* Version: 1.0.0
* [Learn Markdown](https://bitbucket.org/nguyenvancanhit/interviewnvc)
* Git pull repo: https://bitbucket.org/nguyenvancanhit/interviewnvc.git

### How do I get set up? ###

* Install: use command line "npm install" to download node_module and build code.
* Start server: use command line "npm start"
* Run tests: http://localhost:3000/
* Database configuration: In foleder src/data/
* Note: You must be run comand line "npm run-script build" when You change code. 
* Thanks :)